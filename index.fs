#! /usr/bin/env sf

class dirent
  variable inode
  variable off
  2 buffer: reclen
  cvariable filetype
  256 buffer: 'filename
  : filename ( -- c-addr u ) 'filename zcount ;
  : visible-file? ( -- f )
    filetype c@ 8 =
    'filename c@ [char] . <> and ;
  : size ( -- d )
    filename r/o open-file throw >r
    r@ file-size throw
    r> close-file throw ;
end-class

FUNCTION: opendir ( z-name -- dir )
FUNCTION: readdir ( dir -- dirent )
FUNCTION: closedir ( dir -- ior )

s" index.html" 2constant index-name

index-name delete-file drop

: |  0 parse type cr ;

: .index ( dir -- )
  begin dup readdir ?dup while
    [OBJECTS dirent names file OBJECTS]
    file visible-file? if
       ." <tr><td><a href='" file filename 2dup type
       ." '>" type ." </a></td><td>"
      file size du. ." </td></tr>
  then repeat drop ;

[BUF
| <!doctype html>
| <meta charset="utf-8">
| <title>Index</title>
| <table><thead><tr><td>Name</td><td>Size</td></tr></thead><tbody id=table>

z" ." opendir constant dir
dir .index
dir closedir drop

| </tbody></table>

| <style>
| thead { background-color: #bbb; }
| </style>
| <script>
| ;(function() {
|   var files = Array.prototype.slice.call(table.getElementsByTagName('tr'))
|   var sortedFiles = files.map(function(tr) {
|     return [tr.children[0].children[0].href, tr]
|   }).sort().map(function(elem) { return elem[1] })
|   sortedFiles.forEach(function(tr) {
|     table.appendChild(tr)
|   })
| })()
| </script>

BUF] @BUF index-name w/o create-file throw constant index-file
index-file write-file throw  index-file close-file throw
bye
