PACKAGE http-requests
package-peeker request.

max-clients table http-request
  http-request-size cell+ aligned rec buffer
  \ none of these should be accessed prior to CHECK returning true
  2 cells rec 'headers
  2 cells rec 'firstline
  cell rec 'method
  2 cells rec 'request-uri
  2 cells rec 'host
  2 cells rec 'user-agent
end-table

symbol 'GET
symbol 'HEAD
symbol 'POST

'GET 'HEAD 'POST 3 symbols: 'METHODS

: clear ( -- )  buffer off ;
: allowed ( u -- u' )  buffer @ http-request-size limit ;
: full? ( -- f )  buffer @ http-request-size >= ;
: receive ( c-addr u -- )  allowed tuck buffer @+ + swap move  buffer +! ;

\ ----------------------------------------
: m:headers ( -- )  -failed   \ 'blah\r\n\r\npost' -- 'post'
  'headers into s\" \r\n\r\n" m:search
  \ include the first newline of the double-newline header terminator
  matched? if 2 matchres +! then ;

: m:method ( -- )  -failed  \ 'GET blah -- 'blah'
  skipping m:word m:space
  matched? if matchres 2@ 'METHODS try-symbols 'method ! then ;

: m:request-uri ( -- )  -failed  \ 'blah HTTP/1.1' -- 'HTTP/1.1'
  'request-uri into m:urichars m:space ;

\ match: ...\r\nHeader: ...\r\nblah -- 'blah'
: m:header ( a '\r\nHeader: ' -- )  -failed
  skipping m:search into m:line ;

\ ----------------------------------------
: header? ( '\r\nHeader: ' -- c-addr u f )
  (matchres) -rot 'headers 2@ matching m:header  matchres 2@ matched? ;
: referer? ( -- c-addr u f )
  s\" \r\nReferer: " header? ;

\ ----------------------------------------
: (header) ( a 'header' -- )  'headers 2@ matching m:header ;
: check ( -- f )
  buffer @+ matching m:headers ?fail
  'headers 2@ matching 'firstline into m:line ?fail
  'firstline 2@ matching m:method m:request-uri ?fail
  'user-agent s\" \r\nUser-Agent: " (header) ?fail
  'host s\" \r\nHost: " (header) ?fail  true ;

\ ----------------------------------------
\ ( -- c-addr u )
: firstline   'firstline 2@ ;
: request-uri 'request-uri 2@ ;
: method      'method @ ;
: ua          'user-agent 2@ ;
: host        'host 2@ ;

END-PACKAGE
