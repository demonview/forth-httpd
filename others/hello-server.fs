0 constant /recv
requires pollfds.fs
requires net.fs

class client
  variable socket
  variable peer
  variable written
  : greet ( -- c-addr u )
    s\" Hello, world!\r\n" written @ /string ;
  : .peer ( -- )  peer @ ?dup if .ip else ." (unknown IP)" then space ;

  : connected ( -- )  peer off  written off  socket @ USING pollfd writing
    socket @ USING pollfd fd @ peername if drop peer ! then ;

  : reap ( -- )
    greet nip if
      ." A client connected from " .peer ." but wasn't fully greeted :("
    else
      ." A client from " .peer ." was greeted."
    then cr ;

  : update ( -- )
    greet socket @ USING pollfd send written +!
    greet nip 0= if reap socket @ USING pollfd kill then ;
end-class

100 constant max-clients
requires pollserver.fs

pollserver builds hello-server

: go ( -- )
  4000 hello-server init
  hello-server /serving ;
