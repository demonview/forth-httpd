requires debug.fs
requires epoll.fs
requires net.fs

variable counter
class client
  variable socket
  variable peer
  variable written
  variable id
  : greet ( -- c-addr u )
    s\" HTTP/1.1 200 OK\r\nConnection: close\r\n\r\nHello, world!\r\n"
    written @ /string ;
  : .peer ( -- )  peer @ ?dup if .ip else ." (unknown IP)" then space ;

  : connected ( event -- )  socket !  counter @ id ! counter ++
    peer off  written off  socket @ USING epoll-event writing
    socket @ USING epoll-event fd peername if drop peer ! then
    ." Received connection #" counter ? cr ;

  : reap ( -- )
    greet nip if
      ." A client connected from " .peer ." but wasn't fully greeted :("
    else
      ." A client from " .peer ." was greeted."
    then cr ;

  : update ( event -- )  socket !
    greet socket @ USING epoll-event send drop written +!
    greet nip 0= if socket @ USING epoll-event kill then ;
end-class

100 constant max-clients
requires epollserver.fs

epollserver builds hello-server

: go ( -- )
  4000 hello-server init
  hello-server /serving ;
