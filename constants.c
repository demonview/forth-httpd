#define _LARGEFILE64_SOURCE
#include <sys/stat.h>
#include <linux/limits.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <signal.h>
#include <linux/net.h>
#include <sys/syscall.h>

#define _con(c,f) printf("%6d CONSTANT " f "\n", c)
#define con(c,f) _con(c,#f)
#define c(k) _con(k,#k)

int main (void) {
  c(O_DIRECTORY);
  c(O_NOFOLLOW);
  c(O_RDONLY);
  c(O_WRONLY);
  c(O_LARGEFILE);
  c(O_APPEND);
  c(O_CREAT);
  c(PATH_MAX);
  c(EPOLL_CLOEXEC);
  c(EPOLLIN);
  c(EPOLLOUT);
  c(EPOLLPRI);
  c(EPOLLHUP);
  c(EPOLLERR);
  c(EPOLLET);
  c(EPOLL_CTL_ADD);
  c(EPOLL_CTL_DEL);
  c(EPOLL_CTL_MOD);
  c(MSG_DONTWAIT);
  c(EAGAIN);
  c(EINTR);
  c(POLLIN);
  c(POLLOUT);
  c(POLLERR);
  c(POLLHUP);
  c(POLLNVAL);
  c(PF_INET);
  c(AF_INET);
  c(SOCK_STREAM);
  c(SOL_SOCKET);
  c(SO_REUSEADDR);

  /* only needed for ciforth */
  c(__NR_fstat);
  c(__NR_openat);
  c(__NR_open);
  c(__NR_getsockname);
  c(__NR_getpeername);
  c(__NR_bind);
  c(__NR_listen);
  c(__NR_socket);
  c(__NR_poll);
  c(__NR_getsockopt);
  c(__NR_setsockopt);
  con(__NR_sendto, __NR_send);
  con(__NR_recvfrom, __NR_recv);
  c(__NR_accept4);

  /* index of UID within the stat struct */
  con(&((struct stat *)0)->st_uid, ST_UID);
  con(&((struct stat *)0)->st_size, ST_SIZE); /* this is wrong */

  /* size in bytes of the UID - emitted as a comment */
  printf("\\ st_uid size: %d\n", sizeof(((struct stat *)0)->st_uid));

  /* size of the stat struct */
  con(sizeof(struct stat), STAT_SIZE);

  return 0;
}
