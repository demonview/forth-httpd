PACKAGE net

PUBLIC

: net. net >order name evaluate previous ; immediate

: getpeername ( sockfd a-sockaddr n-socklen -- !f ) __NR_getpeername XOS ;
: getsockname ( sockfd a-sockaddr n-socklen -- !f ) __NR_getsockname XOS ;
: bind ( sockfd addr addrlen ) __NR_bind XOS ;
: listen ( sockfd backlog ) 0 __NR_listen XOS ;
: socket ( domain type protocol ) __NR_socket XOS ;
: poll ( fds nfds timeout ) __NR_poll XOS ;
: (accept) ( sockfd addr addrlen ) 0 0 __NR_accept4 XOS5 ;
: getsockopt ( sockfd level optname optval optlen ) __NR_getsockopt XOS5 ;
: setsockopt ( sockfd level optname optval optlen ) __NR_setsockopt XOS5 ;

: ntohs ( $ABCD -- $CDAB )
  dup  $FF and 8 lshift
  swap 8 rshift or ;

: ntohl ( $ABCD1234 -- $3412CDAB )
  dup  $FFFF and ntohs 16 lshift
  swap 16 rshift ntohs or ;

PRIVATE

2 table sockaddr
  1 rec sin_family
  1 rec sin_family'
  1 rec sin_port
  1 rec sin_port'
  2 cells 4 + aligned rec sin_addr
end-table

: port ( -- port ) sin_port w@ ntohs ;
: ip ( -- ip ) sin_addr @ ;

\ u-ip should have reversed bytes: $0100007F for 127.0.0.1
: server-socket! ( u-ip u-port -- )
  PF_INET sin_family C!  0 sin_family' C!
  DUP 8 RSHIFT sin_port C!  255 AND sin_port' C!
   ( ip ) sin_addr ! ; \ works in 64 and 32 bit both

: serversock  0 sockaddr ;
: clientsock  1 sockaddr ;

: accept ( fd1 -- ior-fd2 )
  [ clientsock ' sockaddr >body @ @ ] literal
  [            ' sockaddr table-size ] literal (accept) ;

\ : ?ERRUR ( x -- | !!! )  -1 = if -10 throw then ;

: tcp-socket ( -- ior-sock )
  AF_INET SOCK_STREAM 0 socket ;

: reuseport ( fd -- )
  1 >R
  SOL_SOCKET SO_REUSEADDR RP@ CELL setsockopt ?ERRUR
  \ dup SOL_SOCKET SO_REUSEPORT RP@ CELL setsockopt ?ERRUR
  r> drop ;

PUBLIC

: server-socket ( port -- sock )
  tcp-socket dup ?ERRUR dup reuseport
  $0100007F rot serversock server-socket!
  dup [ serversock ' sockaddr table-addr ] literal
      [ serversock ' sockaddr table-size @ ] literal
  bind ?ERRUR
  dup 4 listen ?ERRUR ;

: peername ( sock -- ip port -1 | 0 )
  [ clientsock ' sockaddr >body @ @ ] literal [ ' sockaddr table-size ] literal getpeername
  if 0 exit then clientsock ip port true ;

: sockname ( sock -- ip port -1 | 0 )
  [ serversock ' sockaddr >body @ @ ] literal [ ' sockaddr table-size ] literal  getsockname
  if 0 exit then serversock ip port true ;

: .DQ ( ip -- )
  dup $FF and 0 .r [char] . emit 8 rshift
  dup $FF and 0 .r [char] . emit 8 rshift
  dup $FF and 0 .r [char] . emit 8 rshift
      $FF and 0 .r ;
: .sockname ( ip port -- )  swap .DQ ." :" 0 .r ;
: .ip ( ip -- ) .DQ ;

end-package
