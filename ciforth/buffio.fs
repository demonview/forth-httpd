package buffio

private

' emit alias orig-emit
' type alias orig-type

$8000 buffer: buf
variable BP

: buf-emit ( c -- )
  BP @ c! BP ++ ;

: buf-type ( c-addr u -- )
  BP @ swap dup BP +! move ;

public

: [buf ( -- )
  buf BP !
  ['] buf-emit ['] emit [ 3 CELLS ] LITERAL MOVE
  ['] buf-type ['] type [ 3 CELLS ] LITERAL MOVE ;

: buf] ( -- )
  ['] orig-emit ['] emit [ 3 CELLS ] LITERAL MOVE
  ['] orig-type ['] type [ 3 CELLS ] LITERAL MOVE ;

: @buf ( -- c-addr u )
  buf BP @ over - ;

end-package
