WANT 2VARIABLE
WANT BOUNDS
WANT -ROT
WANT /STRING
WANT TRUE
WANT TIME&DATE
WANT ALIAS
WANT CASE-INSENSITIVE
WANT ?EXIT
WANT SLITERAL
WANT >=
WANT DEFER
WANT [IF]
WANT :NONAME
WANT WORD
WANT VOCABULARY
WANT $-PREFIX
WANT LOCAL
WANT 0>
CASE-INSENSITIVE

: +USER ( size "name" -- ) CREATE ALLOT ;

: [VFX]     0 postpone [IF] ; immediate
: [SF]      0 postpone [IF] ; immediate
: [GF]      0 postpone [IF] ; immediate
: [IFORTH]  0 postpone [IF] ; immediate
: [CI]     -1 postpone [IF] ; immediate

: VFX:    10 parse 2drop ; immediate
: SF:     10 parse 2drop ; immediate
: GF:     10 parse 2drop ; immediate
: IFORTH: 10 parse 2drop ; immediate
: CI:                   ; immediate

: fstat ( fd a-buf -- ior )  0 __NR_fstat XOS ;
: open  ( z flags mode -- fd )  __NR_open XOS ;

: append-file ( c-addr u -- fd ior )
  [ O_APPEND O_CREAT or O_LARGEFILE or O_WRONLY or ] literal
  open-file ;

create <EOL>
  1 c,
  10 c,
  0 c,
  0 c,

: FOURTH ( a _ _ _ -- a _ _ _ a )  2over drop ;
: THIRD ( a _ _ -- a _ _ a )  dup fourth nip ;
: 4DUP ( d d -- d d d d )  2over 2over ;
: ON ( a -- )  -1 SWAP ! ;
: OFF ( a -- )  0 SWAP ! ;
: ++ ( a -- )  1 SWAP +! ;
1 CELLS CONSTANT CELL
: OCTAL ( -- )  8 BASE ! ;
: @EXECUTE ( a -- )  @ DUP IF EXECUTE ELSE DROP THEN ;
: W@ ( a -- u-16bit )
  COUNT SWAP C@ 8 LSHIFT OR ;
: W! ( u-16bit a -- )
  OVER 8 RSHIFT OVER 1+ C! C! ;

' NAME ALIAS PARSE-WORD
' , ALIAS COMPILE,
' RSP@ ALIAS RP@

: ZLENGTH ( z -- c-addr u )  \ XXX: used by paths.fs where this makes some sense
  DUP PATH_MAX 0 $^ OVER - ;
: PLACE ( c-addr u a -- )
  2DUP C! 1+ SWAP MOVE ;
: ZPLACE ( z a -- )
  >R ZLENGTH R> PLACE ;

: N>R ( ... n -- ) ( -- R: ... n )
  POSTPONE dup POSTPONE begin POSTPONE dup POSTPONE while
    POSTPONE rot POSTPONE >r POSTPONE 1-
  POSTPONE repeat POSTPONE drop POSTPONE >r ;
IMMEDIATE

: BUFFER: ( u "name" -- )  CREATE ALLOT ;

256 BUFFER: ESCAPE-TABLE
: | ( c c "name" -- )  name 2drop swap escape-table + c! ;
  &a 7 | BEL
  &b 8 | BS
  &e 27 | ESC
  &f 12 | formfeed
  &l 10 | linefeed
  &n 10 | newline
  &q &" | quote"
  &" &" | quote
  &r 13 | cr
  &t 9  | horiztab
  &v 11 | verttab
  &z  0 | NUL
  &\ &\ | backslash

\ NB. escapes usually do nothing when an invalid character is
\ backlashed.  This replaces invalid escapes with garbage bytes.
2VARIABLE A
: >A ( c -- )  A 2@ + C!  1 A +! ;
: ESCAPE ( c-addr u -- c-addr u' )
  over [ A CELL+ ] literal !  0 A !  bounds ?do
    i c@ &\ = if i 1+ c@
      dup &m = if drop 13 >A 10 >A  2 else
          &x = if      i 2 + c@ i 1+ c@ 4 lshift or >A  3 else
      i 1+ c@ escape-table + c@ >A  2 then then
    else i c@ >A  1
  then +loop  A 2@ ;

: S\" ( -- c-addr u' )
  [char] " parse POSTPONE skip escape $,
  POSTPONE literal POSTPONE $@ ;
IMMEDIATE

: .\" ( -- )
  ['] S\" EXECUTE POSTPONE TYPE ;
IMMEDIATE

: $=(NC) ( c-addr u c-addr u -- f )
  rot over <> if drop 2drop 0 exit then
  cora-ignore 0= ;

: PREFIX? ( 'string' 'str' -- f )
  rot over min -rot $= ;

: PREFIX?(NC) ( 'string' 'STR' -- f )
  rot over min -rot $=(NC) ;

: PREFIX-BOUNDS ( 'needle' 'haystack ' -- 'needle' lim start )
  bounds >r over - 1+ r> ;

: SEARCH ( 'haystack' 'needle' -- 'match' f )
  2over prefix-bounds ?do
    2dup i over $= if
      2drop over i swap - /string true unloop exit
    then
  loop 2drop false ;

: SEARCH(NC) ( 'haystack' 'needle' -- 'match' f )
  2over prefix-bounds ?do
    2dup i over $=(NC) if
      2drop over i swap - /string true unloop exit
    then
  loop 2drop false ;

' SSE ALIAS NOW

\ NB. PP isn't line-based
' PP ALIAS >IN

' $@ ALIAS @+

: DIE ( -- )  1 _ _ __NR_exit XOS ;

( -- )
: bright .\" \e[31;1m" ;
: normal .\" \e[0m" ;

