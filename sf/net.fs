requires tcpip

PACKAGE TCP-HOST

PRIVATE

FUNCTION: getpeername ( sockfd a-sockaddr n-socklen -- !f )
FUNCTION: getsockname ( sockfd a-sockaddr n-socklen -- !f )

class sockaddr
  hvariable 'family
  hvariable 'port
  variable 'ip
  : port ( -- port ) 'port w@ ntohs ;
  : ip ( -- ip ) 'ip @ ntohl ;
end-class

sockaddr builds addrbuf
create addrbuflen  sockaddr sizeof ,

PUBLIC


98 constant EADDRINUSE

socket: <socket>
: server-socket ( port -- sock )
  >r <socket> /tcp-socket r> reuseaddr server /bind /listen
  SFD @ ;
: wut 4000 server-socket . sfd my-sa 12 dump ;

: accepted ( -- sock ) CFD @ ;

: peername ( sock -- ip port -1 | 0 )
  [ addrbuf addr ] literal addrbuflen getpeername
  if 0 exit then addrbuf ip addrbuf port true ;

: sockname ( sock -- ip port -1 | 0 )
  [ addrbuf addr ] literal addrbuflen getsockname
  if 0 exit then addrbuf ip addrbuf port true ;

: .sockname ( ip port -- )  swap (.DQ) TYPE ." :" 0 .r ;
: .ip ( ip -- ) (.DQ) TYPE ;

END-PACKAGE
