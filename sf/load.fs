\ The following breaks under SwiftForth
\   $ sf include foo.fs program foo bye
\   $ ./foo include bar.fs program bar bye
\ 'foo' isn't able to create 'bar'
\ or rather, it isn't without /home/SwiftForth existing ...

\ thus, this file.

include config.fs
include constants.fs
include sf/compat.fs
include misc.fs
include chibi.fs

include debug.fs
s" HTTP-DEBUG" find-env nip nip 0= [IF]
  debugging? off
[THEN]

include sf/net.fs
include sf/time.fs
include symbol.fs
include matcher.fs
include paths.fs
include routing.fs
include http-request.fs

s" HTTPENGINE" find-env drop s" EPOLL" compare 0= [IF]
  include epoll/epoll.fs
  include httpd.fs
  include routes.fs
  include epoll/epollserver.fs
[ELSE] s" HTTPENGINE" find-env drop s" THREAD" compare 0= [IF]
  include poll/pollfds.fs
  include httpd.fs
  include routes.fs
  include thread/threadserver.fs
[ELSE]
  include poll/pollfds.fs
  include httpd.fs
  include routes.fs
  include poll/pollserver.fs
[THEN] [THEN]
