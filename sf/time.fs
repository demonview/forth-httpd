PACKAGE ctime

PRIVATE

FUNCTION: time ( a -- time_t )
FUNCTION: ctime ( a-time_t -- z )

PUBLIC

: now ( -- n )  0 time ;
: .now ( n -- c-addr u ) sp@ ctime nip zcount 1- type ;

END-PACKAGE
