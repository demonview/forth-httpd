PACKAGE epoll-events
package-peeker events.
package-peeker socket.

PUBLIC
[VFX]
  AliasedExtern: epoll-create int epoll_create1 ( int flags );
  AliasedExtern: epoll-ctl int epoll_ctl ( int epfd, int op, int fd, char * event );
  AliasedExtern: epoll-wait int epoll_wait ( int epfd, char * events, int maxevent, int timeout );
  AliasedExtern: write-socket int send ( int fd, int addr, int len, int flags );
  AliasedExtern: read-socket int recv ( int fd, int addr, int len, int flags );
[ELSE] [SF]
  AS epoll-create FUNCTION: epoll_create1 ( flags -- ior )
  AS epoll-ctl FUNCTION: epoll_ctl ( epfd op fd event -- ior )
  AS epoll-wait FUNCTION: epoll_wait ( epfd events maxevents timeout -- ior )
  AS write-socket FUNCTION: send ( fd addr len flags -- ior )
  AS read-socket FUNCTION: recv ( fd addr len flags -- ior )
[THEN]


defer fail:epoll
:noname ( ior -- ) abort" epoll failed!" ; is fail:epoll
PRIVATE

\ epoll differs from poll in one big respect, which, shockingly, is not
\ documented, although a code example requires this behavior.  I didn't
\ learn this except through debugging earlier code that was unaware of it.

\ that respect: epoll reorders the buffer you give it.  When N fds have
\ updates, the first N members of the buffer will correspond to those fds.

\ as this breaks any address or index-based relationship (like the mirrored
\ arrays of pollserver.fs), epoll accompanies the event-flag results with
\ 64 bits of user data, which is documented as a C union.

\ we use those 64 bits for: a 32-bit address, a 16-bit epoll fd, and
\ our 16-bit fd.  strictly speaking, 16 bits should not be enough for an
\ fd, but it works provided that we don't have more than 32k open files
\ and provided that Linux doesn't suddenly develop randomized FDs or
\ something.

\ if this arrangement needs to be changed, probably the user data should
\ contain only an address, and any extra data can go in the referenced
\ object.

PUBLIC
VFX: : h! ( this is wrong ) w! ;
VFX: : h@ ( this is wrong ) w@ ;
PRIVATE

max-clients 1+ table epoll-event
  cell rec events
  cell rec data
  2 rec epfd
  2 rec 'fd
end-table

: fd ( -- fd ) 'fd h@ ;

: ctl ( epfd op -- )  fd [ ' epoll-event >body ] literal @ epoll-ctl fail:epoll ;
: add ( epfd -- )  dup epfd h!  EPOLL_CTL_ADD ctl ;
: ready? ( -- flike )  events @ EPOLLIN EPOLLOUT or EPOLLPRI or and ;
: dying? ( -- flike )  events @ EPOLLHUP EPOLLERR or and ;
: remove ( -- )  epfd h@ EPOLL_CTL_DEL ctl  fd close-file drop  -1 'fd h! ;
: clear? ( -- f )  fd 0< ;
: kill ( -- )  EPOLLHUP events ! ;

: server? ( -- f )  data @ 0= ;
: writing ( -- )  EPOLLOUT events !  epfd h@ EPOLL_CTL_MOD ctl ;

: recv ( c-addr u -- c-addr u f-block? )
  fd third rot MSG_DONTWAIT read-socket
  dup 0> if false else errno @ EAGAIN = then ;
: send ( c-addr u -- u f-block? )
  fd -rot MSG_DONTWAIT write-socket
  dup 0> if false else errno @ EAGAIN = then ;

[SF]
  : bar ( n x -- n' x )  over if ." |" then swap 1+ swap ;
  : (ev) ( n mask POLLx 'POLLx' -- n' mask' )
    2>r 2dup and if invert and bar 2r> type else drop 2r> 2drop then ;
  : ev  parse-word 2dup evaluate  postpone sliteral  postpone (ev) ; immediate
  : .events ( -- )
    0 events @
    ev EPOLLIN  ev EPOLLOUT  ev EPOLLPRI
    ev EPOLLHUP  ev EPOLLHUP  ev EPOLLET
    ?dup if bar nip 0 .r else drop then ;
[THEN]

END-PACKAGE
