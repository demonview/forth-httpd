[DEFINED] clients 0= [IF] abort
  package clients
  max-clients 2+ table client
  end-table
  : connected ( -- ) socket is live ;
  : reap ( -- ) clean up, log death, etc.  socket will be killed afterwards ;
  : update ( -- ) you have input!  (or may now write).  deal with it ;
  end-package

[THEN]

package epollserver
package-peeker server.

: socket->client ( -- )
  socket. data @
    dup client. client
        request. http-request ;

: client ( index -- )
  socket. epoll-event
  socket->client ;

variable epoll
variable next-client
variable incoming
variable 'server
variable #clients

: init-with ( socket -- )
  0 socket. epoll-event
  dup 'server !  socket. 'fd h!
  EPOLLIN socket. events !  socket. data off ( false -> server )
  EPOLL_CLOEXEC epoll-create dup 0< fail:epoll  dup epoll !
  socket. add  next-client off  #clients off
  max-clients 0 do
    i socket. epoll-event
    i socket. data !
  loop ;
: init ( port -- )  server-socket init-with ;

: server ( -- server-socket )  'server @ ;

defer too-many-connections ( socket -- )
:noname close-file drop ; is too-many-connections

: /preaccepting ( -- )
  DEBUG: next-client @ abort" unhandled accept"
  socket. ready? if
    SF: /accept accepted
   VFX: server net. accept dup ?ERRUR
    #clients @ max-clients >=
    if too-many-connections else next-client ! then
  else [SF] socket. .events [THEN] true abort" bad server" then ;

: /accepting ( -- )
  next-client @ ?dup if next-client off
    incoming @ socket. epoll-event socket->client
    socket. 'fd h!  EPOLLIN socket. events !
    socket->client
    epoll @ socket. add
    client. connected
    #clients ++
  then ;

: /updating ( n-max -- )
  0 ?do i socket. epoll-event
    socket. server? if /preaccepting else
    socket. ready? if
      socket->client
      client. update
    then
    socket. dying? if
      socket->client
      client. reap
      socket. remove
      -1 #clients +!
    then then
  loop ;

: poll ( -- n ) epoll @ [ epoll-events >order ' epoll-event table-addr previous ] literal #clients @ 1+ -1 epoll-wait ;

\ 'poll' in this definition is the method
: /serving ( -- )
  begin
    poll dup incoming ! dup 0< if drop errno @ EINTR <> abort" epoll_wait failed"
    else /updating /accepting then
  again ;

end-package
