help:
	@echo make all         : build constants.fs and index.html
	@echo make vfx         : build vfx executables
	@echo make lina        : build ciforth executables
	@echo make sf          : load SwiftForth with core and an engine
	@echo make clean       : clean all constructed files
	@echo make distclean   : clean ALL constructed files
	@echo
	@echo -- these assume a running server on port 4000 --
	@echo make getflood    : spam server with GET requests
	@echo make spamflood   : spam server with pseudorandom garbage
	@echo make engines     : test localhost with apachebench
	@echo make engines-single : same, but one connection at a time

all:: constants.fs constants32.fs index.html

vfx::
	make -f Makefile.vfx vfxh vfx-poll vfx-epoll
	make -f Makefile.vfx hello-website simple-website game-website

sf::
	make -f Makefile.sf sf-poll

lina:: ciforth/package.fs
	make -f Makefile.lina lina-poll
	make -f Makefile.vfx hello-website simple-website game-website

iforth::
	make -f Makefile.if iforth

clean::
	rm -fv vfxh vfx-poll vfx-epoll
	rm -fv sfh sf-poll sf-epoll sf-thread
	rm -fv lina-core lina+ linah lina-poll
	rm -fv hello-website simple-website game-website

SWOOP=VFXharness.fth SwoopPortable.f SFharness.f rndoop.f swoop.zip
SWOOP+=iforth-harness.fs package-iforth.fs
SWOOP:=$(foreach x,$(SWOOP),swoop/$x)
distclean:: clean
	rm -fv constants.fs constants32.fs constants64.fs index.html access.log
	rm -fv lib/package.fs ciforth/package.fs
	rm -fv $(SWOOP)
	-rmdir swoop

constants32.fs: constants.c
	[[ ! -x ./a.out ]]
	cc -m32 $<; ./a.out | tee $@; rm -f a.out

constants.fs: constants.c
	[[ ! -x ./a.out ]]
	cc $<; ./a.out | tee $@; rm -f a.out

index.html::
	sf index.fs

lib/package.fs:
	-mkdir lib
	wget -O $@ http://theforth.net/package/package/current/package.fs

ciforth/package.fs: lib/package.fs
	wget -O $@ http://theforth.net/package/package/current/compat-ciforth.fs

swoop/package-iforth.fs:
	wget -O $@ http://theforth.net/package/package/current/package-iforth.fs

swoop/iforth-harness.fs: swoop/package-iforth.fs
	wget -O $@ http://theforth.net/package/swoop-compat/current/iforth-harness.fs

swoop/SwoopPortable.f:
	-mkdir swoop
	wget -O swoop/swoop.zip http://soton.mpeforth.com/flag/swoop/swoop.zip
	( cd swoop; unzip swoop.zip )

getflood::
	@( sleep 5; for x in {1..100}; do for y in {1..50}; do curl http://localhost:4000 >/dev/null 2>/dev/null & done; done ) &

spamflood:
	@( sleep 5; for x in {1..100}; do for y in {1..50}; do cat /dev/urandom | nc localhost 4000 >/dev/null 2>/dev/null & done; done ) &

n=30
test::
	ab -n 10000 -c $n -r http://localhost:4000/index.html

engines-single:
	make n=1 _engines-test

engines:
	make n=30 _engines-test

_engines-test:
	make realclean engine=poll config all
	./http-server 4000 &
	echo -e "\e[32;1m"
	ab -n 10000 -c $n -r http://localhost:4000/index.html
	pkill -KILL http-server
	echo -e "\e[0m"

	make realclean engine=epoll config all
	./http-server 4000 &
	echo -e "\e[33;1m"
	ab -n 10000 -c $n -r http://localhost:4000/index.html
	pkill -KILL http-server
	echo -e "\e[0m"

	make realclean engine=thread config all
	./http-server 4000 &
	echo -e "\e[34;1m"
	ab -n 10000 -c $n -r http://localhost:4000/index.html
	pkill -KILL http-server
	echo -e "\e[0m"
