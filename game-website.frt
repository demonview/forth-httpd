\ serve static files out of public/ , and perform some extra functions
\ for the the javascript in those files 

routing
  rewrite / /index.html

  :noname request. ua s" Googlebot" search nip nip ;
  :noname client. kill ;

  when /time
  :noname [OK client. created @ .now OK] ;

  variable counter
  when /counter
  :noname [OK counter ++ counter @ 0 .r OK] ;

  when /ip
  :noname [OK client. peer @ .ip OK] ;

  static-rule

  404-rule
end-routing

[CI]
  : go ( -- )
    ." Serving files from the current directory on http://127.0.0.1:4000/" cr
    4000 server. init  server. /serving ;
[ELSE]
requires admin.fs

: go ( -- )
  cr bright ." Logging to access.log" normal
  s" access.log" log

  z" public" chdir if
    cr bright ." Unable to chdir into public/ directory -- bailing out!"
    normal die
  then go ;
[THEN]
