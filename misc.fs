: >order ( wl -- )
  >r get-order 1+ r> swap set-order ;

[CI]
  : (package-peeker) ( wl wl "name" -- wl wl )
    create dup ,
    does> @ >order parse-word evaluate previous ;
  : package-peeker ( wl wl "name" -- wl wl )  public (package-peeker) immediate private ;
[ELSE]
  : package-peeker ( "name" -- )
    public create immediate  private get-current ,
    does> @ >order parse-word evaluate previous ;
[THEN]


: limit ( n-want n-sofar n-max -- n-allowed )
  - over + 0 max - ;
