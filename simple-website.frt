\ serve static files out of public/ if it exists, and the
\ current directory otherwise.  logs to stdout.  interactive
\ Forth is still present for admin tasks.

routing
  rewrite / /index.html

  static-rule

  404-rule
end-routing

[CI]
  : go ( -- )
    ." Serving files from the current directory on http://127.0.0.1:4000/" cr
    4000 server. init  server. /serving ;
[ELSE]
requires admin.fs

: go ( -- )
  cr ." Serving files from "
  z" public" chdir if ." the current directory" else ." public/" then go ;
[THEN]
