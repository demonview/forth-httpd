[DEFINED] program-name [IF]
  : start-server ( -- )  server. /serving ;
  : die ( -- )  1 exitstatus ! bye ;
  : usage ( -- )
    ." usage: " program-name type ."  <port>" die ;
  ( continued below )
[ELSE]
  [SF]
    0 task httpd-task
    : start-server ( -- )
      httpd-task activate  server. /serving ;
    : die ( -- ) abort ;
  [THEN] [VFX]
    task httpd-task
    : start-server ( -- )
      httpd-task start: server. /serving ;
    : die ( -- ) abort ;
  [THEN]
[THEN]

: log ( 'filename' -- )  append-file throw to logfile ;

create server-port 4000 ,
: port ( port -- )  server-port ! ;

: (go) ( -- )  server-port @ server. init ;
: go ( -- )
  cr ['] (go) catch if errno @
    EADDRINUSE = if ." Error: address already in use"
    else ." Server failed to start"
  then cr die then
  server. server socket. fd sockname if ." Started listening on " .sockname
  else ." Server started listening" then cr start-server ;

\ can't use [BUF BUF] as the server's using it in the same thread
max-clients 2* cell+ buffer: statusbuf
: type-to ( c-addr u a -- )
  2dup  @+ +  2swap +!  swap move ;
: emit-to ( c a -- )
  tuck  @+ +  c!  ++ ;

: status ( -- )  statusbuf off
  max-clients 0 ?do
    i 25 mod 0= if <EOL> count statusbuf type-to then
    i server. client  client. statuscode statusbuf emit-to
  loop  statusbuf @+ page type ;

: watch ( -- )
  begin key? 0= while
    status
  50 ms repeat key drop ;

[defined] program-name [if]
  : (main) ( -- )
    cmdline ['] number catch if usage then port
    s" LOGFILE" find-env if log else 2drop then ;
  : leaving ( ior -- )
    0<> 1 or exitstatus !
    bright program-name type ."  stopped." normal bye ;
[then]
