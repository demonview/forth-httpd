needs toolbelt

: [VFX]     0 postpone [IF] ; immediate
: [SF]      0 postpone [IF] ; immediate
: [GF]      0 postpone [IF] ; immediate
: [IFORTH] -1 postpone [IF] ; immediate
: [CI]      0 postpone [IF] ; immediate

: VFX:    0 parse 2drop ; immediate
: SF:     0 parse 2drop ; immediate
: GF:     0 parse 2drop ; immediate
: IFORTH:               ; immediate
: CI:     0 parse 2drop ; immediate

create <EOL>
  1 c,
  10 c,
  0 c,
0 c,

256 BUFFER: ESCAPE-TABLE
: | ( c c "name" -- )  parse-name 2drop swap escape-table + c! ;
  &a 7 | BEL
  &b 8 | BS
  &e 27 | ESC
  &f 12 | formfeed
  &l 10 | linefeed
  &n 10 | newline
  &q &" | quote"
  &" &" | quote
  &r 13 | cr
  &t 9  | horiztab
  &v 11 | verttab
  &z  0 | NUL
  &\ &\ | backslash

2VARIABLE A
: >A ( c -- )  A 2@ + C!  1 A +! ;
: ESCAPE ( c-addr u -- c-addr u' )
  over [ A CELL+ ] literal !  0 A !  bounds ?do
    i c@ &\ = if i 1+ c@
      dup &m = if drop 13 >A 10 >A  2 else
          &x = if      i 2 + c@ i 1+ c@ 4 lshift or >A  3 else
      i 1+ c@ escape-table + c@ >A  2 then then
    else i c@ >A  1
  then +loop  A 2@ ;

: ,\" ( "string" -- )
  [char] " parse escape string, ;

( -- )
create 'bright ,\" \e[31;1m"
create 'normal ,\" \e[0m"
: bright 'bright count type ;
: normal 'normal count type ;
