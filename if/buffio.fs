package buffio

private

'emit @ constant 'orig-emit
'type @ constant 'orig-type

$8000 buffer: buf
variable BP

: buf-emit ( c -- )
  BP @ c! BP ++ ;

: buf-type ( c-addr u -- )
  BP @ swap dup BP +! move ;

public

: [buf ( -- )
  buf BP !
  ['] buf-emit 'emit !
  ['] buf-type 'type ! ;

: buf] ( -- )
  'orig-emit 'emit !
  'orig-type 'type ! ;

: @buf ( -- c-addr u )
  buf BP @ over - ;

end-package
