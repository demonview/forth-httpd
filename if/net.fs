FUNCTION: getpeername ( sockfd a-sockaddr n-socklen -- !f )
FUNCTION: getsockname ( sockfd a-sockaddr n-socklen -- !f )
Extern: uint16_t ntohs ( uint16_t netshort );
Extern: uint32_t ntohl ( uint32_t netshort );
Extern: int bind ( int sockfd, const char * addr, int addrlen );
Extern: int listen ( int sockfd, int backlog );
Extern: int socket ( int domain, int type, int protocol );
Extern: int poll ( char * fds, int nfds, int timeout );
AliasedExtern: (accept) int accept ( int sockfd, char * addr, int * addrlen );
Extern: int getsockopt ( int sockfd, int level, int optname, char * optval, int optlen );
Extern: int setsockopt ( int sockfd, int level, int optname, const char * optval, int optlen );

\ seems like iforth's 'C server' would need to be extended
\ doable by rsyncing from expected location and patching it...

\ a project for another time
