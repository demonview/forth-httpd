requires time.fs
variable last-fork

\ if the watched program can't stay up for longer than 5 seconds,
\ give up on it.
: -too-unstable ( --f )
  now last-fork @ - 5 < if
    ." Watched program is too unstable -- letting it die." cr bye
  then ;

: spawn ( -- )
  0 pad z" -c" $SHELL sp@ over swap 'env execve
  ." exec failed" bye ;

: watch ( pid -- )
  exitstatus 0 waitpid drop ;

: forever ( -- )
  cmdline pad zplace
  begin now last-fork !
    fork ?dup if watch else spawn then
  -too-unstable again ;
