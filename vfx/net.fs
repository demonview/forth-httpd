PACKAGE net
package-peeker net.

PUBLIC

FUNCTION: getpeername ( sockfd a-sockaddr n-socklen -- !f )
FUNCTION: getsockname ( sockfd a-sockaddr n-socklen -- !f )
Extern: uint16_t ntohs ( uint16_t netshort );
Extern: uint32_t ntohl ( uint32_t netshort );
Extern: int bind ( int sockfd, const char * addr, int addrlen );
Extern: int listen ( int sockfd, int backlog );
Extern: int socket ( int domain, int type, int protocol );
Extern: int poll ( char * fds, int nfds, int timeout );
AliasedExtern: (accept) int accept ( int sockfd, char * addr, int * addrlen );
Extern: int getsockopt ( int sockfd, int level, int optname, char * optval, int optlen );
Extern: int setsockopt ( int sockfd, int level, int optname, const char * optval, int optlen );

PRIVATE

2 table sockaddr
  1 rec sin_family
  1 rec sin_family'
  1 rec sin_port
  1 rec sin_port'
  2 cells 4 + aligned rec sin_addr
end-table

: port ( -- port ) sin_port w@ ntohs ;
: ip ( -- ip ) sin_addr @ ;

\ u-ip should have reversed bytes: $0100007F for 127.0.0.1
: server-socket! ( u-ip u-port -- )
  PF_INET sin_family C!  0 sin_family' C!
  DUP 8 RSHIFT sin_port C!  255 AND sin_port' C!
   ( ip ) sin_addr ! ; \ works in 64 and 32 bit both

: serversock  0 sockaddr ;
: clientsock  1 sockaddr ;
create addrbuflen  ' sockaddr table-size @ ,

: accept ( fd1 -- ior-fd2 )
  [ clientsock ' sockaddr table-addr ] literal addrbuflen (accept) ;

PUBLIC
: ?ERRUR ( x -- | !!! )  -1 = if -10 throw then ;
PRIVATE

: tcp-socket ( -- ior-sock )
  AF_INET SOCK_STREAM 0 socket ;

: reuseport ( fd -- )
  1 >R
  SOL_SOCKET SO_REUSEADDR RP@ CELL setsockopt ?ERRUR
  \ dup SOL_SOCKET SO_REUSEPORT RP@ CELL setsockopt ?ERRUR
  r> drop ;

PUBLIC

: server-socket ( port -- sock )
  tcp-socket dup ?ERRUR dup reuseport
  $0100007F rot serversock server-socket!
  dup [ serversock ' sockaddr table-addr ] literal
      [ serversock ' sockaddr table-size @ ] literal
  bind ?ERRUR
  dup 4 listen ?ERRUR ;

: peername ( sock -- ip port -1 | 0 )
  [ clientsock ' sockaddr table-addr ] literal addrbuflen getpeername
  if 0 exit then clientsock ip port true ;

: sockname ( sock -- ip port -1 | 0 )
  [ serversock ' sockaddr table-addr ] literal addrbuflen getsockname
  if 0 exit then serversock ip port true ;

: .DQ ( ip -- )
  dup $FF and 0 .r [char] . emit 8 rshift
  dup $FF and 0 .r [char] . emit 8 rshift
  dup $FF and 0 .r [char] . emit 8 rshift
      $FF and 0 .r ;
: .sockname ( ip port -- )  swap .DQ ." :" 0 .r ;
: .ip ( ip -- ) .DQ ;

end-package
