package buffio

private

require %lib%/Genio/Buffer.fth

textbuff: buf
variable orig-op
$8000 buffer: buf-buffer

expose-module genio-buffer
: 0buf ( -- ) \ reset write count back to 0
  buf-buffer $8000 -1 buf open-tb 2drop ;
previous

public

: [buf ( -- )
  0buf
  op-handle @ orig-op !
  buf op-handle ! ;

: buf] ( -- )
  orig-op @ op-handle ! ;

expose-module genio-buffer
: @buf ( -- c-addr u )
  buf gen-handle @ buf tb-#written ;
previous

end-package
