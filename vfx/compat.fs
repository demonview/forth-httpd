require %lib%/Lin32/MultiLin32

: [VFX]    -1 postpone [IF] ; immediate
: [SF]      0 postpone [IF] ; immediate
: [GF]      0 postpone [IF] ; immediate
: [IFORTH]  0 postpone [IF] ; immediate
: [CI]      0 postpone [IF] ; immediate

: VFX:                  ; immediate
: SF:     0 parse 2drop ; immediate
: GF:     0 parse 2drop ; immediate
: IFORTH: 0 parse 2drop ; immediate
: CI:     0 parse 2drop ; immediate

: third ( a b c -- a b c a ) dup 2over drop nip ;
: ++ ( a -- )  1 swap +! ;
: @execute ( a -- )  @ dup if execute else drop then ;
synonym requires require
synonym parse-word parse-name

Extern: int fstat ( int fd, char * buf );

: append-file ( c-addr u -- fd ior )
  [ O_APPEND O_CREAT or O_LARGEFILE or O_WRONLY or ] literal
  open-file ;

create <EOL>
  1 c,
  10 c,
  0 c,
  0 c,

( -- )
: bright .\" \e[31;1m" ;
: normal .\" \e[0m" ;
