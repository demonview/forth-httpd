PACKAGE ctime

PRIVATE

Extern: char * ctime_r( const time_t * timep, char * buf );

\ user-supplied buffer which should have room at least for
\ 26 bytes
128 buffer: ctimebuf

PUBLIC

: now ( -- n )  0 time ;
: .now ( n -- c-addr u ) sp@ ctimebuf ctime_r nip zcount 1- type ;

END-PACKAGE
