variable debugging?
debugging? on

\ consumes remainder of line if debugging? is false
: DEBUG: ( "line of code" -- )
  debugging? @ ?exit
  0 parse 2drop ; immediate

variable n
: ~~ postpone .s n @ postpone literal n ++ postpone . postpone cr ;
immediate
