PACKAGE routes

variable route-depth

PUBLIC

defer route

: routing ( -- )
  depth route-depth !
  routes >order ;

PRIVATE

: end-routing ( xt xt ... xt -- )
  depth route-depth @ - n>r
  :NONAME
    r> begin dup while 1-
      r> COMPILE,
      dup if 1-
        r@ ['] noop = if r> drop else
          POSTPONE if r> COMPILE, POSTPONE exit POSTPONE then
        then
      then
    repeat drop
  POSTPONE ; is route previous ;

END-PACKAGE
