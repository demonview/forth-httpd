PACKAGE routes

: try-file ( -- f )
  request. request-uri evil-path? if false exit then
  request. request-uri 1 /string r/o open-file if drop false exit then
  client. fd ! true ;

: deliver-file ( -- )
  200 client. status !
  [BUF .\" HTTP/1.1 200 OK\r\nContent-Length: "
    client. fd @ file-size drop <# #s #> type
    .\" \r\nConnection: close\r\n\r\n" BUF]
  @BUF client. >write ;

: when ( "path" -- xt )
  :noname
    parse-word POSTPONE sliteral
    [ http-requests >order ] POSTPONE request-uri [ previous ]
    POSTPONE compare POSTPONE 0= POSTPONE ; ;

: rewrite ( 'path' 'path' -- xt xt )
  :noname [ http-requests >order ] 
    POSTPONE request-uri parse-word POSTPONE sliteral
    POSTPONE compare POSTPONE 0= POSTPONE if
      parse-word POSTPONE sliteral POSTPONE 'request-uri POSTPONE 2!
    POSTPONE then
  POSTPONE ; [ previous ]
  ['] noop ;

: [OK ( -- ) [BUF ;
: OK] ( -- ) BUF] @BUF client. ok-response ;


: static-rule ( -- xt xt ) ['] try-file ['] deliver-file ;

: (404-rule) ( -- )
  404 s\" HTTP/1.1 404 Not Found\r\n\r\n" client. fail-with ;
: 404-rule ( -- xt ) ['] (404-rule) ;

END-PACKAGE
