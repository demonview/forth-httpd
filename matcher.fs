\ ----------------------------------------------------------------------
\ matcher.fs
\ --

SF: #USER
  2 cells +USER (matchres)
  2 cells +USER matchstr
  1 cells +USER matching?
  1 cells +USER 'matchres
SF: TO #USER
: matchres ( -- a )  'matchres @ ;

: into ( a -- )  dup off 'matchres ! ;
: skipping ( -- )  (matchres) into ;
skipping

: matching ( c-addr u -- )  matching? on  matchstr 2!  skipping ;
: matched? ( -- f )  matching? @ ;

: failed ( -- ) matching? off ;
: ?fail ( -- f )  \ exit with false if failed
  postpone MATCHED? postpone 0=
  postpone IF postpone FALSE postpone EXIT postpone THEN ; immediate
: -failed ( -- )
  postpone MATCHED? postpone 0= postpone ?EXIT ; immediate

\ ----------------------------------------------------------------------

[CI]
\ match: 'abc' 'a' -- 'bc'
: match ( 'a' -- )  matchstr 2@ LOCAL n1 LOCAL a1 LOCAL n2 LOCAL a2  -failed
  n2 n1 > if failed exit then
  a1 n2 a2 n2 compare if failed exit then
  a1 n1 n2 /string  matchstr 2! ;
[ELSE]
\ match: 'abc' 'a' -- 'bc'
: match ( 'a' -- )  matchstr 2@ locals| n1 a1 n2 a2 |  -failed
  n2 n1 > if failed exit then
  a1 n2 a2 n2 compare if failed exit then
  a1 n1 n2 /string  matchstr 2! ;
[THEN]

: m" [char] " parse postpone SLITERAL postpone MATCH ; immediate

: digit? ( c -- f )    [char] 0 [ char 9 1+ ] literal within ;
: word? ( c -- f )     bl or [char] a [ char z 1+ ] literal within ;
: alphanum? ( c -- f ) dup word? swap digit? or ;
: nonctrl ( c -- f )   bl >= ;
: urichar ( c -- f )   [ bl 1+ ] literal $80 within ;

\ match: '123abc' -- 'abc'
: m:number ( -- )  -failed  matchstr 2@
  0 0 2over bounds do
    i c@ digit? if BASE @ * i c@ [char] 0 - +  swap 1+ swap
  else leave then loop ( 'abc' #digits n )
  over if matchres ! /string matchstr 2! exit then 2drop 2drop failed ;

[CI]
\ match off chars for which xt is true
: m:char-xt ( xt -- )  LOCAL xt   -failed  matchstr 2@
  2dup 0 -rot bounds do
    i c@ xt execute if 1+
  else leave then loop ( 'string' #matched )
  dup if dup matchres ! third matchres cell+ ! /string matchstr 2! exit then
  drop 2drop failed ;

\ match: 'hello' 'el' -- 'lo' )
: m:search ( 'el' -- )  matchstr 2@ LOCAL n1 LOCAL a1 LOCAL n2 LOCAL a2  -failed
  a1 n1 a2 n2 search if
    tuck n2 /string
    rot a1 n1 rot - matchres 2!
  matchstr 2! exit then 2drop failed ;
[ELSE]
\ match off chars for which xt is true
: m:char-xt ( xt -- )  locals| xt |  -failed  matchstr 2@
  2dup 0 -rot bounds do
    i c@ xt execute if 1+
  else leave then loop ( 'string' #matched )
  dup if dup matchres ! third matchres cell+ ! /string matchstr 2! exit then
  drop 2drop failed ;

\ match: 'hello' 'el' -- 'lo' )
: m:search ( 'el' -- )  matchstr 2@ locals| n1 a1 n2 a2 |  -failed
  a1 n1 a2 n2 search if
    tuck n2 /string
    rot a1 n1 rot - matchres 2!
  matchstr 2! exit then 2drop failed ;
[THEN]

\ match: ' hi ' -- 'hi'
: m:space ( -- )  m"  " ;

\ match: 'hello world' -- ' world'  , etc.
: m:word ( -- ) ['] word? m:char-xt ;
: m:alphanum? ( -- ) ['] alphanum? m:char-xt ;
: m:urichars ( -- ) ['] urichar m:char-xt ;

: m:end ( -- )  matchstr @ if failed then ;

defer m:newline

: m:cr  s\" \r" match ;
: m:nl  s\" \n" match ;

\ accepts \r or \n or \r\n as a newline
: m:lax-newline ( -- )  -failed
  s\" \r\n" match  matched? ?exit  matching? on
  s\" \r" match    matched? ?exit  matching? on
  s\" \n" match ;

: m:strict-newline ( -- )  s\" \r\n" match ;

: lax-newlines ( -- )  ['] m:lax-newline is m:newline ;
: strict-newlines ( -- )  ['] m:strict-newline is m:newline ;

strict-newlines

\ match: 'hello\r\nthere' -- 'there
: m:line ( -- )  -failed
  ['] nonctrl m:char-xt m:newline ;
