routing
  :noname [OK .\" Hello!\n" OK] ;
end-routing


[CI]
  : GO ( -- )
    ." Serving greetings to any HTTP request on http://127.0.0.1:4000/" cr
    4000 server. init  server. /serving ;
[ELSE]
  requires admin.fs
  s" /dev/null" log
[THEN]
