\ symbol 'foo
\ symbol 'bar
\ 'foo 'foo =  \ true
\ 'foo 'bar =  \ false
\ 'foo .symbol  \ Output: foo

[CI]
  : symbol ( "name" -- )
    >IN @ NAME ROT >IN !  CREATE 1 /string $, DROP ;

  ' $@ alias symbol>string

  : .symbol ( sym -- )
    $@ TYPE ;
[ELSE]
  : symbol ( "name" -- )
    >in @ parse-word rot >in !  create 1 /string string, ;

  : symbol>string ( sym -- c-addr u )  count ;

  : .symbol ( sym -- )
    count type ;
[THEN]

: symbols: ( sym1 sym2 ... symN N "name" -- )
  create dup , 0 ?do , loop ;

[CI]
  : try-symbols ( c-addr u symbols -- sym | 0 )
    @+ cells bounds do 2dup
      i @ $@ compare 0= if 2drop i @ unloop exit then
    cell +loop 2drop false ;
[ELSE]
  : try-symbols ( c-addr u symbols -- sym | 0 )
    @+ cells bounds do 2dup
      i @ count compare 0= if 2drop i @ unloop exit then
    cell +loop 2drop false ;
[THEN]
