#! /usr/bin/env perl
use common::sense;
die "usage: ./test | ./test.pl\n" if -t \*STDIN;
chomp(my @line = <STDIN>);
if ($line[$#line] eq "\e[0m") { pop @line; pop @line } # sf
my $time = time;
die if $line[0] ne 'Wed Dec 31 18:00:00 1969'
    || $line[1] ne 'Tue Nov  8 13:54:47 2016'
    # yeah, race condition possible here
    || $line[2] != $time
    || $line[3] != $time
    || @line != 4;
print "ok\n";
