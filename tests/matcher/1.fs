2variable hello
2variable world

s" hello, world!" matching
hello into s" ," m:search
s"  " match
world into s" !" m:search

matched? 0= [IF] cr ." Match failed!" [THEN]
hello 2@ s" hello" compare [IF] cr ." 'hello' match failed!" [THEN]
world 2@ s" world" compare [IF] cr ." 'world' match failed!" [THEN]
