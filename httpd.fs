0 value logfile

PACKAGE clients
package-peeker client.

max-clients table client
  cell rec reading \ or writing?
  cell rec created \ when
  cell rec fd      \ source file
  cell rec written \ bytes sent
  cell rec status  \ response status code
  cell rec peer    \ connected IP
  /write cell+ aligned rec buffer
end-table

: statuscode ( -- c )
  fd @ 0= if [char] _ exit then
  fd @ -1 = if [char] . exit then
  reading @ if [char] R else [char] W then ;

: connected ( -- )
  request. clear  reading on  now created !  fd on  written off
  status off  peer off  buffer off
  socket. fd peername if drop peer ! then ;

: qt [char] " emit ;
\ ip [time] "method request http" status written s "referer" "ua"
: log ( -- )
  [BUF peer @ ?dup if .ip space else ." - " then
  ." [" created @ .now ." ] "
  qt request. firstline type qt space
  status ? written @ ?dup if . else ." - " then
  now created @ - .
  qt request. referer? if type else 2drop ." -" then qt space
  qt request. ua type qt cr BUF]
  @BUF logfile write-file drop ;

: kill ( -- )
  socket. kill
  fd @ ?dup if close-file drop  fd on then ;

: reap ( -- )  log ;

: fail-with ( status c-addr u -- )
  written @ if rot negate status !  socket. send 2drop
  else socket. send 2drop  status !  then  kill ;
: 400-bad  400 s\" HTTP/1.1 400 Bad Request\r\n\r\n" fail-with ;
: 408-bad  408 s\" HTTP/1.1 408 Request Timed Out\r\n\r\n" fail-with ;
: 416-bad  416 s\" HTTP/1.1 416 Invalid Request\r\n\r\n" fail-with ;
: 418-bad  418 s\" HTTP/1.1 418 Nothing read\r\n\r\n" fail-with ;
: 431-bad  431 s\" HTTP/1.1 431 Request Header Too Large\r\n\r\n" fail-with ;
: 500-err  500 s\" HTTP/1.1 500 Internal Server Error\r\n\r\n" fail-with ;

: allowed ( u -- u' )  buffer @ /write limit ;
: allowed? ( u -- f )  dup allowed = ;
: remainder ( -- c-addr u )  buffer @+ + /write allowed ;

: write ( -- f )  buffer @+
  2dup socket. send >r
  dup written +!  dup negate buffer +!
  /string buffer cell+ swap move r> ;

: >write ( c-addr u -- )
  dup allowed? 0= if 500-err exit then
  tuck buffer @+ + swap move  buffer +! ;

: from ( -- )  fd @ -1 = ?exit
  remainder fd @ read-file if drop 500-err exit then
  ?dup if buffer +! else fd @ close-file drop fd on then ;

: too-old? ( -- f ) now created @ - 2 > ;

: writing ( -- ) reading off ;

: ok-response ( c-addr u -- )
  200 status !  s\" HTTP/1.1 200 OK\r\n\r\n" >write >write writing ;

: update-writing ( -- )
  written @  begin from
     buffer @ 0= if kill drop exit then
     write
  until
  written @ = if \ failed to write anything?
    fd @ 0> if 500-err exit then
    buffer @ if 500-err exit then
    kill
  then ;

: update ( -- )
  socket. dying? if 418-bad exit then
  reading @ if
    begin pad 4096 socket. recv 0= while
      dup 0= if 2drop exit then
      request. receive
      request. check if
        route writing update-writing exit
      then
      request. full? if 431-bad exit then
    repeat
  else update-writing then ;

END-PACKAGE
