[defined] client 0= [if] abort
  class client
    variable socket
    : connected ( -- ) socket is live ;
  end-class
[then]

PACKAGE TASKING
PUBLIC
: tasks: ( n "array" -- )
  create 0 ?do
    0 , 0 , 0 , tasks >link
  loop does> ( n -- ) swap  4 cells * + ;
END-PACKAGE

max-clients 1+ tasks: tasks

client subclass threaded-client
  variable task
  : /serve ( -- )  task @ activate  connected
    begin socket @ 1 -1 poll 0< if reap kill bye then update again ;
end-class

class threadserver
  max-clients 1+ pollfd builds[] pollfds
  max-clients 1+ threaded-client builds[] clients

  : init-with ( socket -- )
    max-clients 1+ 0 do
      i pollfds init
      i pollfds addr i clients socket !
      i tasks i clients task !
    loop  max-clients pollfds 'fd ! ;
  : init ( port -- )  server-socket init-with ;

  : server ( -- server-socket )  max-clients pollfds fd ;
  : accept? ( -- f )  max-clients pollfds ready? ;
  : size ( -- n )  0 max-clients 0 do i pollfds alive? if 1+ then loop ;

  DEFER: too-many-connections ( socket -- )  close-file drop ;
  : add ( socket -- )
    max-clients 0 do
      i pollfds dead? if
        i pollfds 'fd !
        i clients /serve
    unloop exit then loop  too-many-connections ;

  : /accepting ( -- )  accept? if /accept accepted add then ;

  \ 'poll' in this definition is the syscall
  : poll ( -- ior )  max-clients pollfds addr 1 -1 poll ;

  \ 'poll' in this definition is the method
  : /serving ( -- )
    begin poll 0< abort" poll failed"
    /accepting again ;
end-class
