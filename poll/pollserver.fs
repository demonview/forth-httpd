[DEFINED] clients 0= [IF] abort
  package clients
  package-peeker client.
  max-clients 1+ table client
  end-table
  : connected ( -- )  pollfd now has a live socket ;
  : reap ( -- ) clean up, log death, etc.  pollfd will be killed afterwards ;
  : update ( -- ) you have input!  (or may now write).  deal with it ;
  end-package
[THEN]

PACKAGE pollserver
package-peeker server.

: client ( index -- )
  dup client. client
  dup request. http-request
      pollfd. pollfd ;
: server ( -- )
  max-clients pollfd. pollfd ;


: init-with ( socket -- )
  server pollfd. init  pollfd. 'fd !
  max-clients 0 do
    i pollfd. pollfd
    pollfd. init
  loop ;
: init ( port -- )  server-socket init-with ;

: accept? ( -- f )  server pollfd. ready? ;
: size ( -- n )  0 max-clients 0 do i pollfd. pollfd  pollfd. alive? if 1+ then loop ;

defer too-many-connections ( socket -- )
:noname close-file drop ; is too-many-connections

  : add ( socket -- )
    max-clients 0 do
      i pollfd. pollfd
      pollfd. dead? if
        i client
        pollfd. 'fd !
        client. connected
        unloop exit
      then
    loop  too-many-connections ;

SF:  : /accepting ( -- )  accept? if /accept accepted add then ;
VFX: : /accepting ( -- )  accept? if [ server pollfd. 'fd ] literal @ net. accept dup ?ERRUR add then ;
CI:  : /accepting ( -- )  accept? if [ server pollfd. 'fd ] literal @ net. accept dup ?ERRUR add then ;

: /updating ( -- )
  max-clients 0 do
    i pollfd. pollfd
    pollfd. alive? if
      pollfd. dying? if i client  client. reap pollfd. kill
      else pollfd. ready? if i client  client. update
  then then then loop ;

: /serving ( -- )
  begin  pollfd. poll  0< abort" poll failed"
  /accepting /updating again ;

end-package
