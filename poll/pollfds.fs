PACKAGE pollfds
package-peeker pollfd.
package-peeker socket.

POLLERR POLLHUP or POLLNVAL or constant POLLBAD
POLLIN POLLOUT or constant POLLREADY

[VFX]
  AliasedExtern: write-socket int send ( int fd, int addr, int len, int flags );
  AliasedExtern: read-socket int recv ( int fd, int addr, int len, int flags );
[THEN] [SF]
  AS write-socket FUNCTION: send ( fd addr len flags -- ior )
  AS read-socket FUNCTION: recv ( fd addr len flags -- ior )
[THEN] [CI]
  : write-socket ( fd addr len flags -- ior ) 0 __NR_send XOS5 ;
  : read-socket ( fd addr len flags -- ior ) 0 __NR_recv XOS5 ;
[THEN]

[SF]
: bar ( n x -- n' x )  over if ." |" then swap 1+ swap ;
: (ev) ( n mask POLLx 'POLLx' -- n' mask' )
  2>r 2dup and if invert and bar 2r> type else drop 2r> 2drop then ;
: ev  parse-word 2dup evaluate  postpone sliteral  postpone (ev) ; immediate
: .event ( mask -- )
  0 swap
  ev POLLIN
  ev POLLOUT
  ev POLLERR
  ev POLLHUP
  ev POLLNVAL
  ?dup if bar nip 0 .r else drop then ;
[THEN]

VFX: : h! ( this is wrong ) w! ;
VFX: : h@ ( this is wrong ) w@ ;
CI: : h! ( this is wrong ) w! ;
CI: : h@ ( this is wrong ) w@ ;

\ 1+: include server fd
max-clients 1+ table pollfd
  cell rec 'fd
  2 rec events
  2 rec revents
end-table

: fd ( -- fd ) 'fd @ ;

: init ( -- )  POLLIN events h!
  [ -1 1 rshift invert ] literal 'fd ! ;
: writing ( -- )  POLLOUT events h! ;
: alive? ( -- f )  fd 0 >= ;
: dead? ( -- f )  fd 0< ;
: bad? ( -- flike )  revents h@ POLLBAD and ;
: dying? ( -- flike ) dead? bad? or ;
: ready? ( -- flike )  revents h@ POLLREADY and ;
\ : read ( -- c-addr u f )  pad dup /recv fd read-file drop dup 0> ;
: write ( c-addr u -- )  alive? if fd write-file drop else 2drop then ;

\ don't use close-file here because VFX gets confused by
\ init's fake negative fd
[VFX]
  : kill ( -- )  fd close drop  init ;
[ELSE]
  : kill ( -- )  fd close-file drop  init ;
[THEN]

[SF]
: .pollfd ( -- )
  ." POLLFD(" fd 0 .r ." , " events h@ .event
  ." , "  revents h@ .event ." ) " ;
[THEN]

[CI]
  : recv ( c-addr u -- c-addr u f-block? )
    fd third rot MSG_DONTWAIT read-socket
    dup 0> if false else [ EAGAIN negate ] literal = 0 swap then ;
  : send ( c-addr u -- u f-block? )
    fd -rot MSG_DONTWAIT write-socket
    dup 0> if false else [ EAGAIN negate ] literal = 0 swap then ;
[ELSE]
  : recv ( c-addr u -- c-addr u f-block? )
    fd third rot MSG_DONTWAIT read-socket
    dup 0> if false else errno @ EAGAIN = then ;
  : send ( c-addr u -- u f-block? )
    fd -rot MSG_DONTWAIT write-socket
    dup 0> if false else errno @ EAGAIN = then ;
[THEN]

: poll ( -- ior )
  [ ' pollfd table-addr ] literal [ max-clients 1+ ] literal -1 poll ;

END-PACKAGE
