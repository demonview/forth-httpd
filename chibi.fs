: table ( n-count "name" -- n a )
  create here 0 , 0 , 0 ,  \ current address, 0th address, record width
  does> ( n-index -- )
    tuck cell+ 2@ -rot * + swap ! ;

: end-table ( n-count a -- )
  here over cell+ !
  2 cells + @ * allot ;

: rec ( n a record-size "name" -- n a )
  over 2 cells +  dup @ >r  +!  dup >r
  : r> POSTPONE literal POSTPONE @ r> POSTPONE literal POSTPONE + POSTPONE ; ;

: table-addr ( xt -- a ) >body cell+ @ ;
: table-size ( xt -- a ) >body 2 cells + ;
