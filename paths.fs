STAT_SIZE buffer: statbuf

\ ends in a slash?
: dirlike? ( 'path" -- f )  1- + c@ [char] / = ;

\ contains a slash?
[CI]
  : pathlike? ( 'string' -- f ) &/ $^ ;
[ELSE]
  : pathlike? ( 'string' -- f ) [char] / scan nip 0<> ;
[THEN]

\ begins with a slash?
: absolute? ( 'path' -- f ) drop c@ [char] / = ;

\ contains a ..
: evil-path? ( 'path' -- f )  s" .." search nip nip ;

: dirname ( 'path/to/file' -- 'path/to/' )
  begin 2dup dirlike? ?exit 1- again ;

: filename ( 'path/to/file' -- 'file' )
  2dup + 1- begin dup c@ [char] / =
    if third - 1+ /string exit then
  1- again ;

[CI]
  : openat ( dirfd pathname flags mode -- fd )
    0 __NR_openat XOS5 ;
[ELSE]
  FUNCTION: openat ( dirfd pathname flags mode -- fd )
[THEN]

: fd-uid ( fd -- uid | 0 )
  statbuf fstat if 0 else [ statbuf ST_UID + ] literal @ then ;

[CI]
  \ ST_SIZE is wrong ... :/
  : file-size ( fd -- ud ior )
    statbuf fstat if 0 else [ statbuf 20 + ] literal @ then 0 0 ;
[THEN]

PATH_MAX buffer: dirpath
PATH_MAX buffer: filepath

\ --
\ open directory component of path (provided it is really a
\ directory, provided it is not a symlink) and then, with reference
\ to that directory, open the file component of path (provided it is
\ not a symlink).
\ --
\ directory and file must both have a given UID.
\ --
\ prior path components can be symlinks or be owned by others.
\ presumably the double-UID checks are good enough; that it is not a
\ concern that malicious symlinks would still allow a user to see a
\ file owned by that user in a directory owned by that user, in a
\ location not normally visible to that user.
\ --
: paranoid-open ( 'path/to/file' uid -- fd ior )
    [CI] LOCAL uid [ELSE] locals| uid | [THEN]
  2dup evil-path? if 2drop -1 0 exit then
  2dup pathlike? 0= if 2drop -1 0 exit then
  2dup filename filepath zplace  dirname dirpath zplace

  dirpath [ O_DIRECTORY O_NOFOLLOW or ] literal 0 open
  dup 0< ?dup ?exit
  dup fd-uid uid <> if close-file drop -1 -1 exit then dup

  filepath [ O_NOFOLLOW O_RDONLY or ] literal 0 openat
  dup 0< ?dup ?exit
  dup fd-uid uid <> if close-file drop -1 -1 exit then

  swap close-file drop 0 ;
